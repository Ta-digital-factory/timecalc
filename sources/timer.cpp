#include "../headers/timer.hpp"

#include <chrono>
#include <iostream>
#include <thread>
#include <unistd.h>
#include <wiringPi.h>

timerBlock::timerBlock(int pinNumber):
	pinNumber_(pinNumber), running_(true), cancelRun_(false)
	{
		std::cout << "\t\t1" << std::endl;
		timerThreading_ = std::thread(&timerBlock::startTimer_, this);
		timerThreading_.join();
	}

void timerBlock::startTimer_()
{
	auto startTime = std::chrono::steady_clock::now();

	while(digitalRead(pinNumber_) == true || cancelRun_)
	{
		usleep(1000);
	}

	auto stopTime = std::chrono::steady_clock::now();
	timelapse_ = std::chrono::duration_cast<std::chrono::milliseconds>(stopTime - startTime).count();
	timelapse_ = timelapse_ / 1000; //convert to seconds
	running_ = false;
}