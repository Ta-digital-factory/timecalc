#include <cstdlib> 
#include <iostream>
#include <string>
#include <thread>
#include <unistd.h>
#include <wiringPi.h>
// https://pinout.xyz/#

#include "../headers/timer.hpp"
#include "../headers/database.hpp"

void inputThread();
void sortThread();

static int sensors[8]{0,2,3,4,5,6,21,22};

extern database myDatabase("/home/iot/database/digifact.sqlite3");

int main(int argc, char *argv[])
{
	wiringPiSetup();
	for(int i=0; i < 8; i++)
	{
		pinMode(sensors[i], INPUT);
	}
	


	/* Open database */
	myDatabase.setupDatabase();
	std::thread inputThreading (&inputThread);
	//std::thread sortThreading (&sortThread);
	while(1)
	{
		sleep(1);
	}
	inputThreading.join();
	//sortThreading.join();
	return 0;
}

void inputThread()
{

	while(1)
	{
		if(digitalRead(sensors[0]) == true)
		{
			std::cout << "Input received on invoer\n\t start new thread!" << std::endl;
			timerBlock inputTrack(sensors[1]);
			while(digitalRead(sensors[0]) == true && !inputTrack.getTimerRuns())
			{
				usleep(20 * 1000);
			}
			std::cout << "inputTrack lapse: '" << inputTrack.getRunTime() << "'" << std::endl;
			myDatabase.writeData("Invoerband", inputTrack.getRunTime());
			inputTrack.~timerBlock();
		}
	
		usleep(20 * 1000);
	}
}

void sortThread()
{
	while(1)
	{
		if(digitalRead(sensors[1]))
		{
			std::cout << "Input received on sort\n\t start new thread!" << std::endl;
			timerBlock sortTrack1(sensors[2]);
			timerBlock sortTrack2(sensors[3]);
			timerBlock sortTrack3(sensors[4]);
			while(digitalRead(sensors[2]) == true && (!sortTrack1.getTimerRuns() && !sortTrack2.getTimerRuns() && !sortTrack3.getTimerRuns()))
			{
				usleep(20 * 1000);
			}
			//if(!sortTrack1.getTimerRuns())
		}
		usleep(20 * 1000);
	}
}

