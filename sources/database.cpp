#include "../headers/database.hpp"

#include <cstdlib> 
#include <iostream>
#include <string>
#include <sqlite3.h> 
#include <mutex>

database::~database()
{
	sqlite3_close(db_);
}

void database::setupDatabase()
{
	rc_ = sqlite3_open(database_, &db_);
	if( rc_ ) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db_));
		exit;
	} else {
		fprintf(stderr, "Opened database successfully\n");
	}
}

void database::writeData(const std::string location, float time)
{
	char *zErrMsg = 0;
	std::string sqlstatement = "INSERT INTO timelapse (location, time) VALUES ('" + location + "','" + std::to_string(time) + "');";
	rc_ = sqlite3_exec(db_, sqlstatement.c_str(), 0, 0, &zErrMsg);
	
	lock_.lock();

	if (rc_ != SQLITE_OK ) {

		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		sqlite3_close(db_);

		exit;
	} else {
		fprintf(stdout, "Added value to database\n");
	}sqlstatement;
 
	
	lock_.unlock();
}
