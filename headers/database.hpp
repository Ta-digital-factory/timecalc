#ifndef DIGITALFACTORY_DATABASE_HPP
#define DIGITALFACTORY_DATABASE_HPP

#include <string>
#include <sqlite3.h> 
#include <mutex>

class database
{
public:
	database(char *database): database_(database) {}
	virtual ~database();

	void setupDatabase();
	void writeData(const std::string location, float time);

private:
	sqlite3 *db_;
	sqlite3_stmt *stmt_;
	char *database_;
	int rc_;
	std::mutex lock_;
};

#endif