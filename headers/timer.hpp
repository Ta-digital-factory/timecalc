#ifndef DIGITALFACTORY_TIMER_HPP
#define DIGITALFACTORY_TIMER_HPP

#include <thread>

class timerBlock
{
public:
	timerBlock(int pinNumber);
	virtual ~timerBlock() = default;

	bool getTimerRuns() const {return running_;}
	void cancelRun() {cancelRun_ = true;}

	float getRunTime() const {return timelapse_;}

private:
	int pinNumber_;
	bool running_;
	bool cancelRun_;

	std::thread timerThreading_;
	float timelapse_;

	void startTimer_();
};

#endif